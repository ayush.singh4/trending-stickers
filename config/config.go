package config

import (
	"github.com/spf13/viper"
)

// Config represents the application configuration.
type Config struct {
	Debug    bool           `mapstructure:"debug"`
	Server   ServerConfig   `mapstructure:"server"`
	Context  ContextConfig  `mapstructure:"context"`
	Database DatabaseConfig `mapstructure:"database"`
}

// ServerConfig represents the configuration for the server.
type ServerConfig struct {
	Address string `mapstructure:"address"`
}

// ContextConfig represents the configuration for the context.
type ContextConfig struct {
	Timeout int `mapstructure:"timeout"`
}

// DatabaseConfig represents the configuration for the database.
type DatabaseConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
	User string `mapstructure:"user"`
	Pass string `mapstructure:"pass"`
	Name string `mapstructure:"name"`
}

// LoadConfig loads the application configuration from config.json.
func LoadConfig() (*Config, error) {
	viper.SetConfigFile("/home/ayush/Desktop/TrendingStickers/config/config.json")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	var config Config
	configerr := viper.Unmarshal(&config)
	if configerr != nil {
		return nil, configerr
	}

	return &config, nil
}
