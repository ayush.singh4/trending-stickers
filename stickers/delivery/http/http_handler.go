package delivery

import (
	"net/http"
	"trendingstickers/stickers/usecase"

	"github.com/labstack/echo/v4"
)

type stickerHandler struct {
	stickerUsecase usecase.StickerUsecase
}

// NewStickerHandler creates a new instance of stickerHandler.
func NewStickerHandler(stickerUsecase usecase.StickerUsecase) *stickerHandler {
	return &stickerHandler{
		stickerUsecase: stickerUsecase,
	}
}

func (sh *stickerHandler) GetTrendingStickersHandler(c echo.Context) error {
	trendingStickers, err := sh.stickerUsecase.GetTrendingStickers()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Failed to fetch trending stickers"})
	}

	return c.JSON(http.StatusOK, trendingStickers)
}
