package repository

import (
	"time"
	"trendingstickers/domain"

	"gorm.io/gorm"
)

// StickerRepository represents the repository for sticker data.
type StickerRepository interface {
	GetTrendingStickers(currentTime time.Time) ([]domain.Sticker, error)
}

type stickerRepository struct {
	db *gorm.DB
}

// NewStickerRepository creates a new instance of StickerRepository.
func NewStickerRepository(db *gorm.DB) StickerRepository {
	return &stickerRepository{
		db: db,
	}
}

func (sr *stickerRepository) GetTrendingStickers(currentTime time.Time) ([]domain.Sticker, error) {
	// Implement your logic here to fetch trending stickers based on the current time from the database.
	var stickers []domain.Sticker
	err := sr.db.Joins("JOIN time_ranges ON stickers.id = time_ranges.sticker_id").
		Where("time_ranges.start_time <= ? AND time_ranges.end_time >= ?", currentTime, currentTime).
		Preload("TimeRanges").
		Find(&stickers).Error
	return stickers, err
}
