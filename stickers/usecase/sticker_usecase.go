package usecase

import (
	"time"
	"trendingstickers/domain"
	"trendingstickers/stickers/repository"
)

// StickerUsecase represents the use case for stickers.
type StickerUsecase interface {
	GetTrendingStickers() ([]domain.Sticker, error)
}

type stickerUsecase struct {
	stickerRepository repository.StickerRepository
}

// NewStickerUsecase creates a new instance of StickerUsecase.
func NewStickerUsecase(stickerRepository repository.StickerRepository) StickerUsecase {
	return &stickerUsecase{
		stickerRepository: stickerRepository,
	}
}

func (su *stickerUsecase) GetTrendingStickers() ([]domain.Sticker, error) {
	currentTime := time.Now().UTC()
	return su.stickerRepository.GetTrendingStickers(currentTime)
}
