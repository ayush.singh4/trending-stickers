-- SQL query to drop the time_ranges table if it exists
DROP TABLE IF EXISTS time_ranges;

-- SQL query to drop the stickers table if it exists
DROP TABLE IF EXISTS stickers;

