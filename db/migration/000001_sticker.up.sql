-- SQL query to create the stickers table
CREATE TABLE stickers (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    priority INT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP
);

-- SQL query to create the time_ranges table
CREATE TABLE time_ranges (
    id INT AUTO_INCREMENT PRIMARY KEY,
    sticker_id INT NOT NULL,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    FOREIGN KEY (sticker_id) REFERENCES stickers (id)
);