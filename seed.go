package main

import (
	"fmt"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Sticker struct {
	ID         uint
	Name       string
	Priority   int
	TimeRanges []TimeRange
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  *time.Time
}

type TimeRange struct {
	ID        uint
	StickerID uint
	StartTime time.Time
	EndTime   time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}

var sampleData = []struct {
	StickerName     string
	StickerPriority int
	TimeRanges      []struct {
		StartTime time.Time
		EndTime   time.Time
	}
}{
	{
		StickerName:     "Happy Sticker",
		StickerPriority: 5,
		TimeRanges: []struct {
			StartTime time.Time
			EndTime   time.Time
		}{
			{
				StartTime: time.Date(2023, 7, 20, 8, 0, 0, 0, time.UTC),
				EndTime:   time.Date(2023, 7, 20, 12, 0, 0, 0, time.UTC),
			},
			{
				StartTime: time.Date(2023, 7, 21, 14, 0, 0, 0, time.UTC),
				EndTime:   time.Date(2023, 7, 21, 18, 0, 0, 0, time.UTC),
			},
		},
	},
	{
		StickerName:     "Sad Sticker",
		StickerPriority: 3,
		TimeRanges: []struct {
			StartTime time.Time
			EndTime   time.Time
		}{
			{
				StartTime: time.Date(2023, 7, 22, 10, 0, 0, 0, time.UTC),
				EndTime:   time.Date(2023, 7, 22, 16, 0, 0, 0, time.UTC),
			},
		},
	},
	// Add more sample data here if needed
}

func main() {
	dsn := "root:root@tcp(127.0.0.1:3306)/stickers?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect to database")
	}

	// Seed the database with sample data
	for _, data := range sampleData {
		sticker := Sticker{
			Name:     data.StickerName,
			Priority: data.StickerPriority,
		}

		for _, tr := range data.TimeRanges {
			timeRange := TimeRange{
				StartTime: tr.StartTime,
				EndTime:   tr.EndTime,
			}

			sticker.TimeRanges = append(sticker.TimeRanges, timeRange)
		}

		if err := db.Create(&sticker).Error; err != nil {
			panic("failed to seed database")
		}
	}

	fmt.Println("Database seeding completed successfully!")
}
