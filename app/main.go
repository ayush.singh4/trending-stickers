package main

import (
	"fmt"
	"log"
	"trendingstickers/config"
	"trendingstickers/domain"
	delivery "trendingstickers/stickers/delivery/http"
	"trendingstickers/stickers/repository"
	"trendingstickers/stickers/usecase"

	"github.com/labstack/echo/v4"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {

	cfg, err := config.LoadConfig()
	if err != nil {
		panic("failed to load configuration")
	}

	// Connect to the database
	// Connect to the database using MySQL driver
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		cfg.Database.User,
		cfg.Database.Pass,
		cfg.Database.Host,
		cfg.Database.Port,
		cfg.Database.Name,
	)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect to database")
	}
	// Perform database migration
	if err := migrateDatabase(db); err != nil {
		panic("failed to migrate database")
	}

	// Initialize repositories and use cases.
	stickerRepository := repository.NewStickerRepository(db)
	stickerUsecase := usecase.NewStickerUsecase(stickerRepository)

	// Initialize Echo server
	e := echo.New()

	// Initialize HTTP handlers.
	stickerHandler := delivery.NewStickerHandler(stickerUsecase)

	// Define HTTP routes.
	e.GET("/v1/trendingStickers", stickerHandler.GetTrendingStickersHandler)

	// Start the server
	port := cfg.Server.Address
	log.Printf("Server listening on address %s\n", port)
	e.Logger.Fatal(e.Start(port))
}

func migrateDatabase(db *gorm.DB) error {
	// Perform database migrations here
	db.AutoMigrate(&domain.Sticker{}, &domain.TimeRange{})
	return nil
}
