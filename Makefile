# Define the path to the .env file
ENV_FILE := .env

# Load the environment variables from .env file
include $(ENV_FILE)
export

# Define the migration commands using environment variables
migrate-up: load-env
	migrate -path db/migration -database "mysql://$$DB_USER:$$DB_PASS@tcp($$DB_HOST:$$DB_PORT)/$$DB_NAME?charset=utf8mb4&parseTime=True&loc=Local" -verbose up

migrate-down: load-env
	migrate -path db/migration -database "mysql://$$DB_USER:$$DB_PASS@tcp($$DB_HOST:$$DB_PORT)/$$DB_NAME?charset=utf8mb4&parseTime=True&loc=Local" -verbose down

.PHONY: load-env migrate-up migrate-down