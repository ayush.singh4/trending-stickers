package domain

import "time"

// Sticker represents the model for a sticker entity.
type Sticker struct {
	ID         uint        `gorm:"primaryKey" json:"id"`
	Name       string      `json:"name"`
	Priority   int         `json:"priority"`
	TimeRanges []TimeRange `gorm:"foreignKey:StickerID" json:"time_ranges"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  *time.Time `sql:"index"`
}

// TimeRange represents a specific time range for which the sticker is trending.
type TimeRange struct {
	ID        uint      `gorm:"primaryKey"`
	StickerID uint      `gorm:"index"`
	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
